SKIP WAITING v. 1.01
a game made with godot

by Jonas Hansen

SKIP WAITING  is played with the mouse only: 
- left or right button to move left or right leg
- left and right button simultaneously to jump 
- holding one button while tapping the other
  to jump in one direction

Sounds by Jonas Hansen and //freesound.org:
150110 | minimal-beatloop by jp2012 CC BY-NC 3.0
135936 | collectcoin by bradwesson CC0 1.0

Font from //fontlibrary.org
Sansus Webissimo by Sergiy S. Tkachenko CC BY 3.0

License: Creative Commons (CC BY-NC 4.0) 
Attribution-NonCommercial 4.0 International 
by Jonas Hansen, 2016, //pixelsix.net