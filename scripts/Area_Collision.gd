extends Area2D

var is_BallInArea = false
var ball = null

func _on_Area_body_enter( body ):
	print("Entered Area2D with body ", body)
	
	if(body.get_name() == "Ball"):
		is_BallInArea = true
		ball = body
		
	print(str(is_BallInArea))


func _on_Area_body_exit( body ):
	print("Exited Area2D with body ", body)
	
	if(body.get_name() == "Ball"):
		is_BallInArea = false
		ball = null
	
	print(str(is_BallInArea))
