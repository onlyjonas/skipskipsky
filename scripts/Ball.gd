extends RigidBody2D

export var kickForce = 100
var skipCount = 0
var bounceForce = 0
var rebound = 0
var global = null
var player = null
var raycast = null
var reset_once = false
var bonus_node = null
var ScoreLabel = null
var once = false
var randSkipNum = 25

func _ready():

	global = get_node("/root/global")
	player = get_node("/root/SceneRoot/Player")
	raycast = get_node("RayCast2D_down")
	ScoreLabel = get_node("/root/SceneRoot/LabelSkipScore")
	raycast.add_exception(self)
	raycast.add_exception(player)
	set_fixed_process(true)
	randSkipNum = int(rand_range(19,25)) 
	
func _fixed_process(delta):

	if(raycast.is_colliding() and !reset_once):
		get_node("SamplePlayer2D").play_random()
	
		if rebound > 0:
			rebound -= 1
			ScoreLabel.set_timed_text("rebound",2)
			kick(0.0, 0.0, 0.0)
			if !rebound > 0:
				get_node("Sprite").set_modulate(Color(255,255,255)) 
		else: 
			reset()
	
	if get_linear_velocity().y < 0:
		# ball moves up
		reset_once = false
		
	if skipCount > randSkipNum and !once:
		print("SKIP COUNT",  skipCount)
		if !bonus_node:
			once = true
			spawn_bonus()

func spawn_bonus():
	var bonus = load("res://scenes/BonusColl.tscn")
	print("----------------------", bonus)
	bonus_node = bonus.instance()
	get_node("/root/SceneRoot").add_child(bonus_node)

func kick(force_horz, force_vert, dist):
	skipCount += 1
	if force_vert == 0:
		force_vert = -0.1
	if bounceForce >= 0.0:
		bounceForce += force_vert
	else:
		bounceForce = 0.0
	
	set_axis_velocity(Vector2(5*force_horz, -bounceForce*kickForce -(100-dist)))
	global.set_skips(skipCount)
		
func activateBonus():
	rebound += 1
	get_node("Sprite").set_modulate(Color(255,0,0))

func reset():
	skipCount = 0
	bounceForce = 0
	reset_once = true

