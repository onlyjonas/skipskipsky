
extends Area2D

export var bonus = 10
var AnimPlayer = null
var ScoreLabel = null
var Ball = null
var active = true

func _ready():
	AnimPlayer = get_node("AnimationPlayer")
	ScoreLabel = get_node("/root/SceneRoot/LabelSkipScore")
	Ball = get_node("/root/SceneRoot/Ball")
	connect("body_enter", self, "collect_bonus")
	set_pos(Vector2(rand_range(50,300),500))

func collect_bonus( body ):
	print(body)
	if(body.get_name() == "Player"):
		Ball.activateBonus()
		ScoreLabel.set_timed_text("rebound bonus",2)
		
		get_node("SamplePlayer2D").play("coin",0)
		
		if AnimPlayer.get_current_animation() != "collect":
			AnimPlayer.play("collect")
			
func delete_me():
	# called from animation track "collect"
	active = false
	self.queue_free()
