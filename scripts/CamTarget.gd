
extends Node2D

var ball = null
var player = null
var camera = null
var xPos = 400
var yPos = 0
var zoom = 1

func _ready():
	ball = get_node("/root/SceneRoot/Ball")
	player = get_node("/root/SceneRoot/Player")
	camera = get_node("Camera2D")
	set_fixed_process(true)

func _fixed_process(delta):
	
	if(player.get_pos().y > ball.get_pos().y):
		yPos = ball.get_pos().y + (player.get_pos().y - ball.get_pos().y)/2
#		zoom = 0.5+((player.get_pos().y - ball.get_pos().y)/500)
	else:
		yPos = player.get_pos().y + ball.get_pos().y - player.get_pos().y
#		zoom = 0.5
	
	if(player.get_pos().x > ball.get_pos().x):
		xPos = ball.get_pos().x + (player.get_pos().x - ball.get_pos().x)/2
	else:
		xPos = player.get_pos().x + ball.get_pos().x - player.get_pos().x
	
	zoom = 0.5 + player.get_pos().distance_to(ball.get_pos())/500
	
	set_pos(Vector2(xPos, yPos))
#	print(zoom)
	camera.set_zoom(Vector2(zoom,zoom))
