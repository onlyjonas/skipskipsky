
extends CanvasLayer

var input_states = preload("res://scripts/input_states.gd")
var btn_restart = input_states.new("btn_restart")
var btn_info = input_states.new("btn_info")

var global = null

func _ready():
	global = get_node("/root/global")
	set_fixed_process(true)

func _fixed_process(delta):
	
	if btn_restart.check() == 1:
		global.setScene("res://scenes/StartScene.tscn")

func set_score_height(height):
	var m = round(height/100)
	var cm = abs(round(height - m*100))
	var txt = str(m) +"."+ str(cm)+"m"
	get_node("LabelScoreText").set_text(txt)
