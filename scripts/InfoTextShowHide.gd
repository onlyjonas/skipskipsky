
extends Node2D

export var showPosX = 500
export var hidePosX = 830
export var animSpeed = 0.1
var newTarget = hidePosX

var input_states = preload("res://scripts/input_states.gd")
var btn_info = input_states.new("btn_info")
var show_hide_toggle = false


func _ready():
	set_process(true)
	
func _process(delta):
	
	if btn_info.check() == 3:
		if(show_hide_toggle):
			print("hide info")
			newTarget = hidePosX
			show_hide_toggle = false
		else:
			print("show info")
			newTarget = showPosX
			show_hide_toggle = true
	
	var newPos = get_pos()
	newPos.x += (( newTarget - newPos.x) * animSpeed)
	set_pos(newPos)		
	


