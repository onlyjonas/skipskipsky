
extends Label
export var animSpeed = 0.1
export var positionOffset = Vector2(0, -200)

var main_cam = null
var main_cam_target = null
var player = null
var time_left = 0
var newPosTarget
var once = false

var startText = ""
var startTextArray = ["Nothing to be done."]

func _ready():
	startTextArray.append("I'm glad to see you back.\nI thought you were gone forever.")
	startTextArray.append("We always find something ... \nto give us the impression we exist?") 
	startTextArray.append("I thought you were gone forever.")
	startTextArray.append("Well? Shall we go?")
	startTextArray.append("I can't go on like this.")
	startTextArray.append("Did I ever leave you?")
	startTextArray.append("Nothing happens. Nobody comes,\nnobody goes. It's awful.")
	startTextArray.append("I'm like that. Either I forget\nright away or I never forget.")
	startText = startTextArray[ceil(rand_range(0,startTextArray.size()-1))]
	
	main_cam = get_node("/root/SceneRoot/CamTarget/Camera2D")
	main_cam_target = get_node("/root/SceneRoot/CamTarget")
	player = get_node("/root/SceneRoot/Player")
	newPosTarget = get_pos()
	
	set_fixed_process(true)


func _fixed_process(delta):
	
	if !once:
		set_timed_text (startText, 5)
		once = true
	
	set_scale(main_cam.get_zoom())
	
	var newPos = get_pos()
	newPos.x += ((newPosTarget.x - newPos.x) * animSpeed);
	newPos.y += ((newPosTarget.y - newPos.y) * animSpeed);
	set_pos(newPos)
	
	if get_text():
		if time_left < 0:
			set_text("")
		else: 	
			time_left -= delta

func set_timed_text (text, time):
	set_pos(player.get_pos())
	newPosTarget = Vector2(player.get_pos().x, main_cam_target.get_pos().y) + positionOffset
	time_left = time
	set_text(text)

