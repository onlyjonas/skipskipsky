
extends Node2D

export var offsetHeight = 540
var global = null 
var maxHeight = 0
var ball = null
var labelHeight = null
var hud = null

func _ready():
	global = get_node("/root/global")	
	ball = get_node("/root/SceneRoot/Ball")
	hud = get_node("/root/SceneRoot/HUD")
	labelHeight = get_node("LabelHeight")
#	maxHeight = abs(get_pos().y-offsetHeight)
	print("maxHeight", maxHeight)

	set_fixed_process(true)
	
func _fixed_process(delta):
	
	# update horz position
	set_pos(Vector2(ball.get_pos().x, get_pos().y))
	
	# update vert if new record
	if ball.get_pos().y < get_pos().y:
		set_pos(Vector2(get_pos().x, ball.get_pos().y))
		maxHeight = abs(get_pos().y -offsetHeight)
		set_height_text(maxHeight)	
		
		
func set_height_text(height):
	var m = round(height/100)
	var cm = abs(round(height - m*100))
	var txt = str(m) +"."+ str(cm)+"m"
	set_scale(get_node("/root/SceneRoot/CamTarget/Camera2D").get_zoom())
	labelHeight.set_text(txt)
	global.set_height(height)
	hud.set_score_height(height)
	


