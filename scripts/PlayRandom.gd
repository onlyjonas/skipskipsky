
extends SamplePlayer2D

export var r_sounds = ["","",""]
var prev_sound = 0

func play_random( ):
	
	if !is_voice_active(prev_sound):
		var new_sound = int(rand_range(0,r_sounds.size()))
		play(r_sounds[new_sound])
		prev_sound = new_sound


