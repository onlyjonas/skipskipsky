extends RigidBody2D

export var animSpeed = 0.5
export var jumpForce = 1000
export var kickForce = 100

var input_states = preload("res://scripts/input_states.gd")
var btn_left = input_states.new("btn_left")
var btn_right = input_states.new("btn_right")

var controlCanvas = null
var Poly_LegLeft = null
var Poly_LegRight = null
var Poly_Head = null
var Poly_Head_Array = null
var KickAreaHead = null
var KickAreaLeft = null
var KickAreaRight = null
var PlayerColl = null
var ScoreLabel = null
var raycast = null

var animLeftTarget = 0.0
var animRightTarget = 0.0
var animLeftState = 0.0
var animRightState = 0.0

var is_jumped = 0
var bonus_kick = 1

func _ready():	
	ScoreLabel = get_node("/root/SceneRoot/LabelSkipScore")
	
	Poly_LegLeft = get_node("Poly_LegLeft")
	Poly_LegRight = get_node("Poly_LegRight")
	Poly_Head = get_node("Poly_Head")
	Poly_Head_Array = Poly_Head.get_polygon()
	KickAreaHead = get_node("KickAreaHead")
	KickAreaLeft = get_node("KickAreaLeft")
	KickAreaRight = get_node("KickAreaRight")
	PlayerColl = get_node("CollisionShapePlayer")
	raycast = get_node("CollisionShapePlayer/RayCast2D_down")
	raycast.add_exception(self)
	set_fixed_process(true)

func _fixed_process(delta):
	
	if btn_left.check() == 0:
		left_kick(0.0)		
	if btn_left.check() == 2:
		left_kick(1.0)
		
	if btn_right.check() == 0:
		right_kick(0.0)	
	if btn_right.check() == 2:
		right_kick(1.0)
	
	check_ball_kick()
	update_head_animation()
	check_is_falling_of_ground()
	
	if is_on_ground():
		if btn_left.check() == 0 or btn_right.check() == 0:
			PlayerColl.set_pos(Vector2(0,25))
			is_jumped = 0

func is_on_ground():
	if raycast.is_colliding():
		return true
	else:
		return false

func collectBonus(bonus):
	bonus_kick = bonus

func check_is_falling_of_ground():
	if get_pos().y > 6000:
		ScoreLabel.set_timed_text("  godot!", 60)

func update_head_animation():
	
	var Poly2DArray = Poly_Head.get_polygon()
	
	if get_linear_velocity().x > 0:
		Poly2DArray.set(2, Vector2(60,10))
		Poly2DArray.set(7, Vector2(0,9))
	elif get_linear_velocity().x < 0:
		Poly2DArray.set(2, Vector2(50,9)) 
		Poly2DArray.set(7, Vector2(-10,10))
		
	Poly_Head.set_polygon(Poly2DArray)	
	

func left_kick(animTarget):
	var Poly2DArray = Poly_LegLeft.get_polygon()
	
	var vecA_passive = Vector2(0,50)
	var vecA_active = Vector2(0,25)
	var vecB_passive = Vector2(0,25)
	var vecB_active = Vector2(-25,0)
	
	if(abs(animLeftState-animTarget)>0.01):
		animLeftState += (( animTarget - animLeftState) * animSpeed);
	else:
		animLeftState = animTarget
		
	Poly2DArray.set(3, vecA_passive.linear_interpolate(vecA_active, animLeftState))
	Poly2DArray.set(4, vecB_passive.linear_interpolate(vecB_active, animLeftState))
	
	Poly_LegLeft.set_polygon(Poly2DArray)		
	
func right_kick(animTarget):
	var Poly2DArray = Poly_LegRight.get_polygon()
	
	var vecA_passive = Vector2(25,25)
	var vecA_active = Vector2(50,0)
	var vecB_passive = Vector2(25,50)
	var vecB_active = Vector2(25,25)
	
	if(abs(animRightState-animTarget)>0.01):
		animRightState += (( animTarget - animRightState) * animSpeed);
	else:
		animRightState = animTarget
			
	Poly2DArray.set(2, vecA_passive.linear_interpolate(vecA_active, animRightState))
	Poly2DArray.set(3, vecB_passive.linear_interpolate(vecB_active, animRightState))
	
	Poly_LegRight.set_polygon(Poly2DArray)	

func check_ball_kick():
	
	if animLeftState > 0.5 and animRightState > 0.5:	
		if is_jumped <= 1:
#			print("Jump")
			set_axis_velocity(Vector2((animRightState-animLeftState)*jumpForce,-jumpForce/2))
			PlayerColl.set_pos(Vector2(0,0))
			is_jumped += 1
		if KickAreaHead.is_BallInArea and !is_on_ground():
			kick_ball(rand_range(-0.5, 0.5), (randf()/2), KickAreaHead)
	
	if animLeftState > 0.0:
#		print("LeftKick")
		if KickAreaLeft.is_BallInArea:
			kick_ball(-1, animLeftState, KickAreaLeft)
	
	if animRightState >0.0:
#		print("RightKick")
		if KickAreaRight.is_BallInArea:
			kick_ball(1, animRightState, KickAreaRight)
			

			
func kick_ball(dir_horz, animState, KickArea):
	if KickArea.ball != null:
		if(KickArea.ball.get_linear_velocity().y >= 0.0): 
			var ballDist = KickArea.get_global_pos().distance_to(KickArea.ball.get_global_pos())  
			
			# check if ball is not blocking
			var horz_force = (1.0 - animState)*dir_horz		
			var vert_force = 1.0 - animState	
			
			KickArea.ball.kick(horz_force, vert_force*bonus_kick, ballDist)
			
			# bonus kick
			if bonus_kick > 1:
				bonus_kick = 1
			

			ScoreLabel.set_timed_text(str(KickArea.ball.skipCount), 2)
			get_node("SamplePlayer2D").play_random()
