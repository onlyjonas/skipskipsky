
extends Label

var global = null

func _ready():
	show_score()
	
func show_score():
	var txt = ""
	global = get_node("/root/global")
	if global.current_best_height > 0:
		var m = round(global.current_best_height/100)
		var cm = abs(round(global.current_best_height - m*100))
		txt += str(m) +"."+ str(cm)+"m"
		
	if global.absolut_best_height > 0:
		var m = round(global.absolut_best_height/100)
		var cm = abs(round(global.absolut_best_height - m*100))
		txt += "\n\nBEST\n" + str(m) +"."+ str(cm)+"m"
	
	set_text(txt)
	
	
