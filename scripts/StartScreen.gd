
extends Label

var input_states = preload("res://scripts/input_states.gd")
var btn_left = input_states.new("btn_left")
var btn_right = input_states.new("btn_right")
var btn_quit = input_states.new("btn_quit")
var global = null

func _ready():
	global = get_node("/root/global")
	# Hide mouse cursor
#	Input.set_mouse_mode(1) 
	set_process(true)
	
func _process(delta):
	if btn_left.check() == 3 or btn_right.check() == 3:
		start()
	
	if btn_quit.check() == 3:
		exit()

func start():
	global.current_best_height = 0
	global.current_best_skips = 0
	global.current_highscore = 0
	get_node("/root/global").setScene("res://scenes/MainWorld.tscn")

func exit():
	get_tree().quit()

