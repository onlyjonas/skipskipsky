extends Node

var viewport_scale
var viewport_res
var currentScene = null

var current_best_height = 0
var absolut_best_height = 0
var current_best_skips = 0
var absolut_best_skips = 0
var current_highscore = 0
var absolut_highscore = 0

var save_data = {"best_height":absolut_best_height, "best_skips":absolut_best_skips, "best_highscore": absolut_highscore}
var save_path = "user://savegame.bin"

func _ready():
#	var viewport = get_node("/root").get_children()[1].get_viewport_rect().size
#	viewport_res = get_node("/root").get_children()[1].get_viewport_rect().size
#	viewport_scale = 400/viewport.y
	currentScene = get_tree().get_root().get_child(get_tree().get_root().get_child_count()-1)
	load_savegame()

func save_savegame():
	var savegame = File.new()
	savegame.open_encrypted_with_pass(save_path, File.WRITE, OS.get_unique_ID())
	save_data["best_height"] = absolut_best_height
	save_data["best_skips"] = absolut_best_skips
	save_data["best_highscore"] = absolut_highscore
	savegame.store_var(save_data)
	savegame.close()
	print("data saved")
	
func load_savegame():
	var savegame = File.new()
	savegame.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
	save_data = savegame.get_var()
	savegame.close()
	absolut_best_height = save_data["best_height"]
	absolut_best_skips = save_data["best_skips"]
	absolut_highscore = save_data["best_highscore"]
	get_node("/root/RootScene/CanvasLayer/Score").show_score()
	print("load data: ", save_data)
	

func setScene(scene):
	currentScene.queue_free()
	var s = load(scene)
	currentScene = s.instance()
	get_tree().get_root().add_child(currentScene)
	
func set_height(score):
	if score > current_best_height:
		current_best_height = score
	if current_best_height > absolut_best_height:
		absolut_best_height = current_best_height
		save_savegame()
	update_highscore()
		
func set_skips(score):
	if score > current_best_skips:
		current_best_skips = score
	if current_best_skips > absolut_best_skips:
		absolut_best_skips = current_best_skips
	update_highscore()
	
func update_highscore():
	current_highscore = current_best_height * current_best_skips 
	if current_highscore > absolut_highscore:
		absolut_highscore = current_highscore
		
		
	
	
	

	